# Ukiyonovels

### TODO
---
[ ] User and token context hooks

[x] Implement costs

[x] Implement complexity for each field / connection / query

[ ] Implement hourly rates

[ ] Add tests

[x] Clean up with codemaid

[ ] Add context hooks for resources

[x] Pageinfo fix

[x] Require first or last && before is not used with after

[x] Cursor decoded guard

[x] Remove graphql-relay

[ ] Add types for cursors and stuff

[ ] Better types

[x] Error classes

[x] Fix build

[x] Create export types

[ ] Dataloader

[ ] CICD

[ ] Testing

[x] JSDOCS

[ ] Logging service

[ ] Deployment

[ ] Update Base{Entity}{Action}Resolver to be a function that could accept a context hook 

---
### Prettier config
```
prettier --print-width 100 --semi true --single-quote --trailing-comma none --write **/*.ts
```